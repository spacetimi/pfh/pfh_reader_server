package main

import (
	"gitlab.com/spacetimi/pfh/pfh_reader_server/app_src/app_init"
	"gitlab.com/spacetimi/shared/timi_shared_server/code/core/shared_init"
	"gitlab.com/spacetimi/shared/timi_shared_server/code/server"
)

func main() {

	shared_init.SharedInit(app_init.GetAppInitializer())

	server.StartServer(&app_init.AppController{})
}
