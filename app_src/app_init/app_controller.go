package app_init

import (
	"gitlab.com/spacetimi/pfh/pfh_reader_server/app_src/app_route_handlers/about"
	"gitlab.com/spacetimi/pfh/pfh_reader_server/app_src/app_route_handlers/faq"
	"gitlab.com/spacetimi/pfh/pfh_reader_server/app_src/app_route_handlers/home"
	"gitlab.com/spacetimi/shared/timi_shared_server/code/core/controller"
)

type AppController struct { // Implements IAppController
}

func (ac *AppController) RouteHandlers() []controller.IRouteHandler {
	return []controller.IRouteHandler{
		home.NewHomeHandler(),
		faq.NewFaqHandler(),
		about.NewAboutHandler(),
	}
}
